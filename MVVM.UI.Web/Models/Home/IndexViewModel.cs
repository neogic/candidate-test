namespace MVVM.UI.Web.Models.Home
{
    public class IndexViewModel
    {
        public string Title { get; set; } = "Home";
        public string Text { get; set; } = "Welcome to the web application.";
    }
}
