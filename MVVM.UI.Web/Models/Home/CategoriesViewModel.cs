namespace MVVM.UI.Web.Models.Home
{
    public class CategoriesViewModel
    {
        public string Title { get; set; } = "Categories";
        public string Text { get; set; } = "Please select modes of transport.";
    }
}
