namespace MVVM.UI.Web.Models.Home
{
    public class FiltersViewModel
    {
        public string Title { get; set; } = "Filters";
        public string Text { get; set; } = "Please adjust filters as appropriate.";
    }
}
