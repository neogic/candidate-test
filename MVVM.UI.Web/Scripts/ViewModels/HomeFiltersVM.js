﻿var HomeFiltersViewModel = function () {
    var self = this;

    self.Colours = ko.observableArray([
        { Value: null, Text: ko.observable("Any") },
        { Value: "Red", Text: ko.observable("Red") },
        { Value: "Yellow", Text: ko.observable("Yellow") },
        { Value: "Green", Text: ko.observable("Green") },
        { Value: "Blue", Text: ko.observable("Blue") },
        { Value: "Black", Text: ko.observable("Black") },
        { Value: "White", Text: ko.observable("White") }
    ]);

    self.Colour = ko.observable();

    self.Sizes = ko.observableArray([
        { Value: null, Text: ko.observable("Any") },
        { Value: "28", Text: ko.observable("28") },
        { Value: "30", Text: ko.observable("30") },
        { Value: "32", Text: ko.observable("32") },
        { Value: "34", Text: ko.observable("34") }
    ]);

    self.Size = ko.observable();

    self.Styles = ko.observableArray([
        { Value: null, Text: ko.observable("Any") },
        { Value: "Bootcut", Text: ko.observable("Bootcut") },
        { Value: "Cropped", Text: ko.observable("Cropped") },
        { Value: "Denim", Text: ko.observable("Denim") },
        { Value: "Loose", Text: ko.observable("Loose") },
        { Value: "Other", Text: ko.observable("Other") },
        { Value: "Regular", Text: ko.observable("Regular") },
        { Value: "Skinny", Text: ko.observable("Skinny") },
        { Value: "Slim", Text: ko.observable("Slim") },
        { Value: "Straight Leg", Text: ko.observable("Straight Leg") },
        { Value: "Super Skinny", Text: ko.observable("Super Skinny") },
        { Value: "Tapered", Text: ko.observable("Tapered") },
        { Value: "Wide Leg", Text: ko.observable("Wide Leg") }
    ]);

    self.Style = ko.observable();
}
