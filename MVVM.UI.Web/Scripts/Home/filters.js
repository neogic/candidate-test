﻿
$(document).on("ready", function () {
    var viewModel = new HomeFiltersViewModel();
    window.filtersVM = viewModel;

    if ($('#filters-container')[0] && !ko.dataFor($('#filters-container')[0])) {
        console.log("Applying bindings - #filters-container ..");
        ko.applyBindings(viewModel, $('#filters-container')[0]);
    };
});
