# Neogic Candidate Technical Test - C#/MVVM

- This test requires .NET Framework 4.8 and either Microsoft Visual Studio (e.g., 2022) or a suitable alternative. 
- The test is not a race, but completion is expected within roughly 1 hour (allowances will be made if candidate is unfamiliar with [Knockout.js](https://knockoutjs.com/documentation/introduction.html)).
- There are no trick questions/tasks, but it's important to follow these instructions carefully.

## Instructions
1. Login to Bitbucket, FORK the repository to your own PRIVATE copy, then clone to a local repo.
2. In Git or VS, create a feature branch off master called "*ft/`<YOUR INITIALS>`-candidate-test*".
3. Complete the tasks below. Commit to your branch AFTER EACH TASK, with a brief description.
4. When tasks are complete, merge-commit to master, push to your BB account, then grant read access to bitbucket@neogic.com.

## Tasks

1.	There is an exception when trying to load the homepage - please fix.

2.	Using *site.css*, centre-align the contents of .container both vertically and horizontally, and spend 5 minutes adding some polish to the overall site appearance.

3.	File *MVVM.Shared\Concatenator.cs* contains a badly implemented method `Concatenate()`. Refactor to improve efficiency.

4.	Add unit test(s) for the above to *MVVM.Shared.Tests.Unit\ConcatenatorTests.cs*.

5.	Using a Knockout.js binding, make the element `#data-textarea` show and hide based on the selection of `#select-list`.

6.	Correct the error in method `retrieveDataFromServer()` in file *MVVM.UI.Web\Scripts\Home\index.js*.

7.	Write a new KO Computed *ListBoxItemsCount()*, and bind the text of `#count-span` to it. The *Categories* page should show the total number of items in the list box at any time.

8.	Using KO, adjust the `<select>` elements on the *Filters* page so that they only have the `multiple` attribute only if the "Any" option isn't selected.
